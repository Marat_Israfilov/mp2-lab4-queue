#include "TQueue.h"
#include <iostream>
using namespace std;

int TQueue::GetNextIndex(int index)
{
	return ++index % MemSize;
}

TData TQueue::Get()
{
	TData tmp;
	if (pMem == nullptr) 
		throw SetRetCode(DataNoMem);
	else if (IsEmpty()) 
		throw SetRetCode(DataEmpty);
	else
	{
		tmp = pMem[Li];
		Li = GetNextIndex(Li);
		DataCount--;
	}
	return tmp;
}

void TQueue::Print()
{
	for (int i = 0; i<DataCount; i++)
		cout << pMem[(i + Li) % MemSize] << ' ';
	cout << endl;
}